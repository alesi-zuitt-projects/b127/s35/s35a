const express = require('express');
const router = express.Router();
const userController = require('../controllers/userControllers')
const auth = require ('../auth');



//Routes for email checking in the DB
router.post('/checkEmail', (req,res) => {
	userController.checkEmailExists(req.body).then(result => res.send(result));
})

router.post('/register', (req,res) => {
	userController.registerUser(req.body).then(result => res.send(result));
})

//routes for authentication
router.post('/login', (req, res) =>{
	userController.loginUser(req.body).then(result => res.send(result));
})

//route for postget details
//auth.veirfy acts as a middleware to ensure that the used is logged in before they can access to their profile
router.get('/details', auth.verify, (req,res) => {
	//uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument.
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile({userId: userData.id}).then(result => res.send(result));
})

//get for details via id

// router.get('/getdetails', auth.verify, (req,res) =>{
// 	userController.getDetailsbyId(req.body).then(result => res.send(result))
// })

//get all users
router.get('/getall', (req, res) =>{
	userController.getAllUsers().then(result => res.send(result))
})


//routes to enroll user to a course
router.post('/enroll', auth.verify, (req,res) => {
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		courseId: req.body.courseId
	}
	userController.enroll(data).then(result => res.send(result))
})
// botbot@mail.com
// bot12345









module.exports = router;