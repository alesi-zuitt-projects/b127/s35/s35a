const Course = require ('../models/Course');



//Creation of course
/*
1.Create a conditional statement to check if user isAdmin
2.create a new course object using mongoose model and the information from the request body and the id from the header.
3.Save the new Course to the database

*/

module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let newCourse = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return newCourse.save().then((course,error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})
	}else{
		return false;
	}
}

//retrieve courses

module.exports.getAllCourses = () => {
	return Course.find().then(result =>{
		return result;
	})
}

//retrieve all active
module.exports.getAllActive = () => {
	return Course.find({isActive : true}).then(result => {
		return result;
	})
}

//retrieve specific course
module.exports.SpecificCourse = (data) => {
	return Course.findOne({name: data.name}).then(result => {
		return result;
	})
}


module.exports.getOneCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then((course,err) =>{
		if(err){
			console.log(err);
			return false;
		} else{
			return course;
		}
	})
}

//update


module.exports.updateCourse = (courseId,data) => {
	if (data.isAdmin) {
		return Course.findByIdAndUpdate(courseId).then((result,error)=>{
			if (error) {
				console.log(error)
				return false;
			} else {
				
				result.description = data.course.description
				result.price = data.course.price
				result.name = data.course.name
				
				return result.save().then((updatedCourse, err)=>{
					if (err) {
						return false;
					} else {
						return updatedCourse;
					}
				})
			}
		})

	}
}

// sir paolo method
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) => {
		if (error){
			return false;
		}else{
			return course;
		}
	})
}


//archive
/*
1. check if admin
2.create variable where the isActive will change into false
3. so we can use findByIdAndUpdate(id, updatedVariable). then error handling if course is not archived, return false, if the course is archived successfully, return true
*/
module.exports.archiveCourse = (reqParams) => {
		let updatedCourse = {
			isActive: false
		} 
		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course,error) =>{
			if(error){
				return false;
			}else{
				return course;
			}
		})
	}
