const User = require ('../models/User');
const Course = require ('../models/Course')
const bcrypt = require('bcrypt');
const auth = require('../auth')

//check if the email already exist

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then((result) =>{
		if (result.length > 0) {
			return true;
		}else{
			return false;
		}
		
	})
};

//registration
/*
Steps:
1.
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	//save
	return newUser.save().then((user, error) => {
		//error
		if(error){
			return false;
		}else {
			return true;
		}
	})
};

//userAuth
/*
steps:
1. check the db if the user email exists
2. compare the password provided in the login form with the passowrd stored in the database
3. generate/ return a json web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		//if user does not exist
		if (result == null){
			return "not registered";
		}else {
			//user exist
			//create a variable "isPasswordCorrect" to return the result of the comparing the login form password and the database password
			//"compareSync" method that we used to compare a non encrypted password from the login form to the encrypted password retrieved from the db and returns "true" or "false" value depending on the result
			//(reqbody.password, result.password) nag ocompare si param1 sa param 2
			//A good practice for boolean variable/ constants is to use the word "is" or "are" at the beginnin in the form of is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if (isPasswordCorrect){
				//generate access token
				//use the createAccessToken method defined in "auth.js" file
				//returning an object back to the frontend
				return {	accessToken: auth.createAccessToken(result.toObject())	}
			}else{
				//password did not match
				return false;
			}
		}
	})
};

// details by id
module.exports.getProfile = (data) => {
	return User.findById( data.userId ).then(result => {
		result.password = "";
		return result;
	})
}

//get

// module.exports.getDetailsbyId = (reqBody) => {
// 	return User.findById(reqBody).then((user,err) => {
// 		if (err){
// 			console.log(err);
// 			return false;
// 		} else{
// 			user.password = ""
// 			return user;
			
// 		}
// 	})
// }

//get all users

module.exports.getAllUsers = () => {
	return User.find().then(result => {
		return result
	})
}


//enroll
/*
1. find the document in the database using the user's Id
2.Add the courseId to the user's enrollment array,
3.save the data in the db 
4. find the document in the database using the course's ID
5. add the user id to the course's enrollees array
6. save the data in the database
7.Error handling(if successful return: true, if failed return: false)
*/
module.exports.enroll = async (data) => {
	//creates an "isUserUpdated" variable and returns true upon successful update otherwise false
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		//add the courseId in the user's enrollment array
		user.enrollments.push({courseId : data.courseId})
	

	//saves the updated user information in the database
	return user.save().then((user,error) => {
		if(error){
			return false;
		}else{
			return true;
		}

	})

	})	//isUserUpdated tellss us if the course was successfully added to the user

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		//add the userId in the courses enrollees array
		course.enrollees.push({userId : data.userId})
		
		return course.save().then((course,error) => {
			if(error){
				return false;
			}else{
				return true;
			}
		})

	})

		if (isUserUpdated && isCourseUpdated){
			return true;
		}else{
			return false;
		}
}

